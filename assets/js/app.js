(function ($) {
    $(document).ready(function(){
        var slidePhoneDesktop = $('.phone-slider').last().bxSlider({
            useCSS: true,
            auto: true,
            controls: false,
            pager: false,
            auto: false
        });
        var slidePhoneMobile = $('.phone-slider').first().bxSlider({
            useCSS: true,
            auto: true,
            controls: false,
            pager: false,
            auto: false
        });
       var slider = $('.Slider__content').bxSlider({
            useCSS: true,
            auto: true,
            controls: false,
            pager: false,
            speed: 300,
            pause: 5000,
            mode: 'vertical',
            onSlideNext: function(el, index, newindex){
                console.log(newindex);
                slidePhoneDesktop.goToSlide(newindex);
                slidePhoneMobile.goToSlide(newindex);
                $('.Slider__page a.active').removeClass('active');
                $('.Slider__page a').eq(newindex).addClass('active');
            }
        });
    });
}(jQuery));